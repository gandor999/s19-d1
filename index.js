console.log("Hello");

// Exponent Operator

console.log("\nExponent Operator");

const firstNum = 8 ** 2;
console.log(">> " + firstNum);



// Exponent ES6 Updates

console.log("\nExponent ES6 Updates");

const secondNum = Math.pow(8, 2);
console.log(">> " + secondNum);


// The Template Literals

console.log("\n\nThe Template Literals");

let name = "Geodor";

// Pre -template literal string

console.log("\nPre -template literal string");

let message = "Hello " + name + "! Welcome to programming!";

console.log("Message without template literals: " + message);

// Using Template Literal (Backticks - ``)

console.log("\nUsing Template Literal (Backticks - ``)");

message = `Hello ${name}! Welcome to programming!`;

console.log(`Message with template literals: ${message}`);

const anotherMessage = `
${name} attended a math competition.
He won it by solving the problem 8 ** 2 with the solution of ${secondNum}
`;

console.log(anotherMessage);

const interestRate = 0.1;
const principal = 1000;


console.log(`The interest on your savings account is: ${principal * interestRate}`);


// Array and Object Destructuring
console.log("\n\nArray and Object Destructuring");


// Array Destructuing
// 		- allows to unpack elements in arrays into distince values

console.log("\n\nArray Destructuing");

const fullName = ["Juan", "Dela", "Cruz"];

// Pre-array destructuing

console.log("\nPre-array Destructuing");

console.log(fullName[0]);
console.log(fullName[1]);
console.log(fullName[2]);

console.log(`Hello ${fullName[0]} ${fullName[1]} ${fullName[2]}! It's nice to meet you`);


// With Array Destructuring

console.log("\nWith Array Destructuring");

const [firstName, middleName, lastName] = fullName;

console.log(`Hello ${firstName} ${middleName} ${lastName}! It's nice to meet you`);


// Object Destructuring - allwos us to unpack peroperties of objects into distinct variables


console.log("\n\nObject Destructuing");

const person = {
	givenName: "Jane",
	maidenName: "Smith",
	familyName: "Rogers"
}

// Pre-object destructuing

console.log("\nPre-object Destructuing");

console.log(person.givenName);
console.log(person.maidenName);
console.log(person.familyName);


// With Object Destructuring

console.log("\nWith Object Destructuring");

const {givenName, maidenName, familyName} = person;

console.log(givenName);
console.log(maidenName);
console.log(familyName);

console.log("\nWith Object Destructuring using Functions")

function getFullName ({givenName, maidenName, familyName}){
	console.log(`${givenName} ${maidenName} ${familyName}`);
}

getFullName(person);




// Arrow Functions - compact alternative syntax to traditional functions

/*
	
	Conventional functions:

		function nameFunction (){
			statements;
		}


	Arrow functions:

		const vairable = () => {
			console.log();
			statements;
		}

*/

console.log("\n\nArrow Functions");


/*function printFullName (firstName, middleInitial, lastName){
	console.log(`${firstName} ${middleInitial} ${lastName}`);
}

printFullName("John", "D", "Smith");*/


const printFullName = (firstName, middleInitial, lastName) => {
	console.log(`${firstName} ${middleInitial} ${lastName}`);
}

printFullName("John", "D", "Smith");


const students = ["John", "Jane", "Judy"];

console.log('\n\nPre - Arrow Function');

/*students.forEach(function(student){
	console.log(`${student} is a student`);
});*/

console.log('\n\nArrow Function');
students.forEach((student) => {
	console.log(`${student} is a student`);
});


/*
	const add = (x, y) => {
		return x + y;
	}

*/


const add = (x, y) => x + y;

console.log(add(5, 8));

let total = add(1, 3);

console.log(total);



// Default Function-Argument Value

console.log('\n\nDefault Function-Argument Value');

const greet = (name = 'User') => {
	return `Good Morning, ${name}`;
}

console.log(greet());
console.log(greet("Geodor"));

const caught = (name = 'Abra') => {
	return `I got you! ${name}!`;
}

console.log(caught());


// Class-based Object Blueprints - allows creationg/instantiation of objects using classes as blueprints

console.log('\n\nClass-based Object Blueprints');

/*
	Syntax:

		class className {

			constructor (objectPropertyA, objectPropertyB){

				this.objectPropertyA = objectPropertyA;

				this.objectPropertyB = objectPropertyB;

				this.objectPropertyC = objectPropertyC;
			}
		}
*/



class Car {
	constructor (brand, name, year){
		this.brand = brand;
		this.name = name;
		this.year = year;
	}
}


const myCar = new Car();
console.log(myCar);


myCar.brand = "Ford";
myCar.name = "Ranger Raptor";
myCar.year = 2021;

console.log(myCar);

const myNewCar = new Car("Toyota", "Vios", 2021);

console.log(myNewCar);